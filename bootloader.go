package vz

/*
#cgo darwin CFLAGS: -x objective-c -fno-objc-arc
#cgo darwin LDFLAGS: -lobjc -framework Foundation -framework Virtualization
# include "virtualization.h"
*/
import "C"
import (
	"fmt"
	"runtime"
)

// BootLoader is the interface of boot loader definitions.
// see: LinuxBootLoader
type BootLoader interface {
	NSObject

	bootLoader()
}

type baseBootLoader struct{}

func (*baseBootLoader) bootLoader() {}

var _ BootLoader = (*LinuxBootLoader)(nil)

// LinuxBootLoader Boot loader configuration for a Linux kernel.
type LinuxBootLoader struct {
	vmlinuzPath string
	initrdPath  string
	cmdLine     string
	pointer

	*baseBootLoader
}

func (b *LinuxBootLoader) String() string {
	return fmt.Sprintf(
		"vmlinuz: %q, initrd: %q, command-line: %q",
		b.vmlinuzPath,
		b.initrdPath,
		b.cmdLine,
	)
}

type LinuxBootLoaderOption func(b *LinuxBootLoader)

// WithCommandLine sets the command-line parameters.
// see: https://www.kernel.org/doc/html/latest/admin-guide/kernel-parameters.html
func WithCommandLine(cmdLine string) LinuxBootLoaderOption {
	return func(b *LinuxBootLoader) {
		b.cmdLine = cmdLine
		cs := charWithGoString(cmdLine)
		defer cs.Free()
		C.setCommandLineVZLinuxBootLoader(b.Ptr(), cs.CString())
	}
}

// WithInitrd sets the optional initial RAM disk.
func WithInitrd(initrdPath string) LinuxBootLoaderOption {
	return func(b *LinuxBootLoader) {
		b.initrdPath = initrdPath
		cs := charWithGoString(initrdPath)
		defer cs.Free()
		C.setInitialRamdiskURLVZLinuxBootLoader(b.Ptr(), cs.CString())
	}
}

// NewLinuxBootLoader creates a LinuxBootLoader with the Linux kernel passed as Path.
func NewLinuxBootLoader(vmlinuz string, opts ...LinuxBootLoaderOption) *LinuxBootLoader {
	vmlinuzPath := charWithGoString(vmlinuz)
	defer vmlinuzPath.Free()
	bootLoader := &LinuxBootLoader{
		vmlinuzPath: vmlinuz,
		pointer: pointer{
			ptr: C.newVZLinuxBootLoader(
				vmlinuzPath.CString(),
			),
		},
	}
	runtime.SetFinalizer(bootLoader, func(self *LinuxBootLoader) {
		self.Release()
	})
	for _, opt := range opts {
		opt(bootLoader)
	}
	return bootLoader
}

var _ BootLoader = (*EFIBootLoader)(nil)

// EFIBootLoader Boot loader configuration for EFI
type EFIBootLoader struct {
	pointer

	*baseBootLoader
}

// NewEFIBootLoader creates a EFIBootLoader with the EFI kernel passed as Path.
func NewEFIBootLoader() *EFIBootLoader {
	bootLoader := &EFIBootLoader{
		pointer: pointer{
			ptr: C.newVZEFIBootLoader(),
		},
	}
	runtime.SetFinalizer(bootLoader, func(self *EFIBootLoader) {
		self.Release()
	})
	return bootLoader
}

func (b *EFIBootLoader) SetVariableStore(variableStore *EFIVariableStore) {
	C.setVariableStoreVZEFIBootLoader(b.Ptr(), variableStore.Ptr())
}

// EFIVariableStore
type EFIVariableStore struct {
	pointer
}

func NewEFIVariableStore(nvram string) (*EFIVariableStore, error) {
	nserr := newNSErrorAsNil()
	nserrPtr := nserr.Ptr()

	nvramPath := charWithGoString(nvram)
	defer nvramPath.Free()
	variableStore := &EFIVariableStore{
		pointer: pointer{
			ptr: C.newVZEFIVariableStoreCreatingAt(nvramPath.CString(), &nserrPtr),
		},
	}
	if err := newNSError(nserrPtr); err != nil {
		return nil, err
	}
	runtime.SetFinalizer(variableStore, func(self *EFIVariableStore) {
		self.Release()
	})
	return variableStore, nil
}

func NewEFIVariableStoreFromPath(nvram string) *EFIVariableStore {
	nvramPath := charWithGoString(nvram)
	defer nvramPath.Free()
	variableStore := &EFIVariableStore{
		pointer: pointer{
			ptr: C.newVZEFIVariableStoreURL(nvramPath.CString()),
		},
	}
	runtime.SetFinalizer(variableStore, func(self *EFIVariableStore) {
		self.Release()
	})
	return variableStore
}
